## Inspection Enrollment Chrome Extension

---------
	
Chrome Extension for creating a new chimney inspection report.

The Inspection Enrollment extension leverages the available Chrome APIs to enhance the productivity of the Chimney Inspection Report application,  by removing the biggest pain point data entry.

**Installation:**

You need to node and npm. At the root of the project type:

```node
npm install
```

**Update Firebase Configuration:**
```
app/scripts/popup.js
```

**Build Project:**

```node
gulp build
```

**Software Used:**

1. Vanilla JavaScript (https://developer.mozilla.org/en-US/docs/Web/JavaScript)
1. Semantic UI (https://semantic-ui.com)
1. Flatpickr (https://flatpickr.js.org/)
1. FireBase (https://www.firebase.com)
1. Chrome Ext APIs (https://developer.chrome.com/extensions/api_index)
1. Gulp (https://gulpjs.com/)
1. Babel (https://babeljs.io/)


----------
**Features:**

- Inspection Auto enrollment
- DOM Data Extracts 

