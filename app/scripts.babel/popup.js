'use strict';

// initialize application
let config = {
    apiKey: 'AIzaSyDXHrlD2JIW76neuClnwW810KxqtFQOuio',
    authDomain: 'inspection-reports-555f2.firebaseapp.com',
    databaseURL: 'https://inspection-reports-555f2.firebaseio.com',
    projectId: 'inspection-reports-555f2',
    storageBucket: 'inspection-reports-555f2.appspot.com',
    messagingSenderId: '729710768339'
};

firebase.initializeApp(config);

login('plugin@gmail.com', 'password');

/**
 * Public Global Variables
 */

let database = firebase.database(),
    fullName, firstName, lastName, address, city,
    state, zip, selectedDates

// Load townships
loadTownships();

generateCrewDropDown();

flatpickr(".flatpickr", {
    dateFormat: "m/d/Y",
    onChange: function (_selectedDates, dateStr) {
        selectedDates = new Date(dateStr).valueOf();
    }
});

// use to close the message
$('.message .close').on('click', function () {
    $(this).closest('.message').transition('fade');
});

let cardContent = document.querySelector('.fluid.card');
cardContent.setAttribute('style', 'display:none');

let createJobElement = document.querySelector('#createJob');
createJobElement.setAttribute('style', 'display:none');

let requestClientDataElement = document.querySelector('#requestClientData');

requestClientDataElement.addEventListener('click', extractClient);
createJobElement.addEventListener('click', handleCreateClientAndJob);

extractClient()

/**
 * Extract Client
 */
function extractClient() {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        var tab = tabs[0];
        chrome.tabs.sendRequest(tab.id, { name: null }, function handler(response) {
            let name, index, endIndex;
            if (response[0].trim().indexOf(',') !== -1) {
                name = response[0].trim().split(',');
                firstName = name[1];
                lastName = name[0];
                fullName = (firstName + " " + lastName).trim();
                index = response[1].toLowerCase().trim() === fullName.toLowerCase().trim() ? 2 : 1;
                endIndex = response[1].toLowerCase().trim() === fullName.toLowerCase().trim() ? 1 : 0;
            } else {
                fullName = response[1];
                firstName = response[1].trim().split(' ')[0];
                lastName = response[1].trim().split(' ')[1];
                index = 2;
                endIndex = 1;
            }

            address = response[index];
            let cityStateZip = response[index + 1].split(',');
            let stateZip = cityStateZip[1].trim().split(' ')
            city = cityStateZip[0];
            state = stateZip[0];
            zip = stateZip[1];

            document.querySelector('#firstName').innerHTML = firstName;
            document.querySelector('#lastName').innerHTML = lastName;
            document.querySelector('#address').innerHTML = response.slice(index, 3 + endIndex).join('</br>');
            document.querySelector('#number').innerHTML = response.slice(4 + endIndex).join('</br>');
            if (response.length > 0) {
                createJobElement.setAttribute('style', 'display:block');
                requestClientDataElement.setAttribute('style', 'display:none');
                cardContent.setAttribute('style', 'display:block');
            }
        });
    });
}

/**
 * Handle Create Job
 */
function handleCreateClientAndJob() {
    // display loader
    loader();

    createClient().then(function (result) {
        // hide loader
        loader();
        if (result === 'error') {
            $('.ui.negative.message').removeClass('hidden');
            $('.ui.positive.message').addClass('hidden');
        } else {
            $('.ui.positive.message').removeClass('hidden');
            $('.ui.negative.message').addClass('hidden');
            resetDisplay();
        }
    });
}

/**
 * Create Client
 * 
 * @returns
 */
function createClient() {
    const date = getDateValue();
    const crew = getCrewValue();
    const updates = {};

    firstName = document.querySelector('#firstName').textContent;
    lastName = document.querySelector('#lastName').textContent;

    if (!isValid(firstName, lastName, address, city, state, zip, date, crew)) {
        return Promise.resolve('error');
    }
    // Get a key for a new client.

    const newJobKey = database.ref().child('job').push().key;


    const clientData = {
        firstName: firstName.trim(),
        lastName,
        address,
        city,
        state,
        zip,
        township: getTownshipValue()
    };

    Object.keys(clientData).forEach(function (key) {
        if (clientData[key] === '' || clientData[key] === false) {
            delete clientData[key];
        }
    });

    return checkIfClientExist(firstName, lastName)
        .then(client => {
            let newClientKey;

            if (client) {
                newClientKey = client.key;
            } else {
                newClientKey = database.ref().child('clients').push().key;
            }

            return getJobCount()
                .then(count => {

                    let jobData = {
                        assigned: crew,
                        client: newClientKey,
                        date,
                        internal: false,
                        inspection: false,
                        status: 0,
                        order: count + 1
                    }

                    updates['/clients/' + newClientKey] = clientData;
                    updates['/jobs/' + newJobKey] = jobData;

                    updateJobCount(count + 1);

                    return database.ref().update(updates);
                })

        })

}

/**
 * Get Township Value
 * 
 * @returns {string}
 */
function getTownshipValue() {
    return $('.ui.dropdown.townships').dropdown('get value');
}

/**
 * Get Date Value
 * 
 * @returns {number}
 */
function getDateValue() {
    return selectedDates;
}

/**
 * Get Crew Value
 * 
 * @returns {string}
 */
function getCrewValue() {
    return $('.ui.dropdown.crews').dropdown('get value');
}

/**
 * Load Townships into select drop down
 * 
 */
function loadTownships() {
    firebase.database().ref('/townships/').once('value').then(function (snapshot) {
        let townships = snapshot.val();
        let dropdown = document.querySelector('.ui.search.dropdown.townships');
        let docfrag = document.createDocumentFragment();
        let townshipsList = townships[Object.keys(townships)[0]]

        townshipsList.forEach((township => {
            let option = document.createElement('option')
            option.innerText = township.trim();
            option.setAttribute('value', township.trim());
            docfrag.appendChild(option);
        }))
        dropdown.appendChild(docfrag);
        $('.ui.dropdown.townships').dropdown()
    });
}

function getJobCount() {
    return firebase.database().ref('/jobCount/').once('value')
        .then((snapshot) => snapshot.val().value)
}

function updateJobCount(count) {
    const update = {};
    update[`/jobCount/value`] = count;
    database.ref().update(update);
}

function checkIfClientExist(_firstName, _lastName) {


    return firebase.database().ref('/clients/').once('value')
        .then((snapshot) => {
            const clients = snapshot.val();
            const keys = clients ? Object.keys(clients) : [];

            let clientsList = keys
                .reduce((acc, key) => {
                    const { firstName, lastName } = clients[key];
                    acc.push({ firstName, lastName, key });
                    return acc;
                }, [])

            return clientsList.find(c => {
                const { firstName, lastName } = c;
                return firstName === _firstName && _lastName === lastName
            })
        })
}

function generateCrewDropDown() {
    firebase.database().ref('/crews/').once('value').then(function (snapshot) {
        let crews = snapshot.val();
        let dropdown = document.querySelector('.ui.search.dropdown.crews');
        let docfrag = document.createDocumentFragment();
        const keys = Object.keys(crews);

        let crewsList = keys
            .reduce((acc, key) => {
                acc.push(crews[key].name);
                return acc;
            }, [])

        crewsList.forEach(((crew, index) => {
            let option = document.createElement('option')
            option.innerText = crew;
            option.setAttribute('value', keys[index]);
            docfrag.appendChild(option);
        }))

        dropdown.appendChild(docfrag);

        $('.ui.dropdown.crews').dropdown()
    });
}

/**
 * Login into Firebase
 * 
 * @param {any} email 
 * @param {any} password 
 */
function login(email, password) {
    firebase.auth().signInWithEmailAndPassword(email, password).catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode === 'auth/wrong-password') {
            alert('Wrong password.');
        } else {
            alert(errorMessage);
        }
        console.log(error);
    });
}

/**
 * Displays network activity loader
 * 
 */
function loader() {
    var loader = document.querySelector('.ui.inverted.dimmer');
    loader.classList.toggle('active');
}

/**
 * Checks each argument to see if it is valid
 * 
 * @returns {any[]}
 */
function isValid() {
    var isValidList = Array.from(arguments).filter(function (arg) {
        var isValid = void 0;
        if (Array.isArray(arg)) {
            isValid = arg.length > 0;
        } else {
            isValid = arg !== '' || arg !== undefined;
        }
        return !isValid;
    });
    return isValidList.length === 0;
}

/**
 * Reset the display
 * 
 */
function resetDisplay() {
    window.close();
    fullName = undefined, address = undefined, city = undefined, state = undefined, zip = undefined, selectedDates = undefined, jobOrderValue = undefined, conditionA = undefined, conditionB = undefined, conditionC = undefined;

    createJobElement.setAttribute('style', 'display:none');
    requestClientDataElement.setAttribute('style', 'display:block');
    cardContent.setAttribute('style', 'display:none');
}