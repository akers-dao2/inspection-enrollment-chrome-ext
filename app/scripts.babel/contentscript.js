/* eslint-disable */
'use strict';

function extractName() {
  let clientName = document.querySelectorAll('#PageContent_lblClientName');
  let clientPageHeaderEl = document.querySelectorAll('.client-page-header :nth-child(1) div.col-10');
  clientName = Array.from(clientName);
  clientPageHeaderEl = Array.from(clientPageHeaderEl);
  let clientInfo = clientName.concat(clientPageHeaderEl);
  return clientInfo.map(client => client.textContent.trim());
}

chrome.extension.onRequest.addListener(
  function (request, sender, sendResponse) {
    sendResponse(extractName())
  });
