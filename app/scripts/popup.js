'use strict';

// initialize application

var config = {
    apiKey: '',
    authDomain: '',
    databaseURL: '',
    projectId: '',
    storageBucket: '',
    messagingSenderId: ''
};

firebase.initializeApp(config);

login('plugin@gmail.com', 'password');

/**
 * Public Global Variables
 */

var database = firebase.database(),
    fullName = void 0,
    firstName = void 0,
    lastName = void 0,
    address = void 0,
    city = void 0,
    state = void 0,
    zip = void 0,
    selectedDates = void 0;

// Load townships
loadTownships();

generateCrewDropDown();

flatpickr('.flatpickr', {
    dateFormat: 'm/d/Y',
    onChange: function onChange(_selectedDates, dateStr) {
        selectedDates = new Date(dateStr).valueOf();
    }
});

// use to close the message
$('.message .close').on('click', function () {
    $(this).closest('.message').transition('fade');
});

var cardContent = document.querySelector('.fluid.card');
cardContent.setAttribute('style', 'display:none');

var createJobElement = document.querySelector('#createJob');
createJobElement.setAttribute('style', 'display:none');

var requestClientDataElement = document.querySelector('#requestClientData');

requestClientDataElement.addEventListener('click', extractClient);
createJobElement.addEventListener('click', handleCreateClientAndJob);

extractClient();

/**
 * Extract Client
 */
function extractClient() {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        var tab = tabs[0];
        chrome.tabs.sendRequest(tab.id, { name: null }, function handler(response) {
            var name = void 0,
                index = void 0,
                endIndex = void 0;
            if (response[0].trim().indexOf(',') !== -1) {
                name = response[0].trim().split(',');
                firstName = name[1];
                lastName = name[0];
                fullName = (firstName + ' ' + lastName).trim();
                index = response[1].toLowerCase().trim() === fullName.toLowerCase().trim() ? 2 : 1;
                endIndex = response[1].toLowerCase().trim() === fullName.toLowerCase().trim() ? 1 : 0;
            } else {
                fullName = response[1];
                firstName = response[1].trim().split(' ')[0];
                lastName = response[1].trim().split(' ')[1];
                index = 2;
                endIndex = 1;
            }

            address = response[index];
            var cityStateZip = response[index + 1].split(',');
            var stateZip = cityStateZip[1].trim().split(' ');
            city = cityStateZip[0];
            state = stateZip[0];
            zip = stateZip[1];

            document.querySelector('#firstName').innerHTML = firstName;
            document.querySelector('#lastName').innerHTML = lastName;
            document.querySelector('#address').innerHTML = response.slice(index, 3 + endIndex).join('</br>');
            document.querySelector('#number').innerHTML = response.slice(4 + endIndex).join('</br>');
            if (response.length > 0) {
                createJobElement.setAttribute('style', 'display:block');
                requestClientDataElement.setAttribute('style', 'display:none');
                cardContent.setAttribute('style', 'display:block');
            }
        });
    });
}

/**
 * Handle Create Job
 */
function handleCreateClientAndJob() {
    // display loader
    loader();

    createClient().then(function (result) {
        // hide loader
        loader();
        if (result === 'error') {
            $('.ui.negative.message').removeClass('hidden');
            $('.ui.positive.message').addClass('hidden');
        } else {
            $('.ui.positive.message').removeClass('hidden');
            $('.ui.negative.message').addClass('hidden');
            resetDisplay();
        }
    });
}

/**
 * Create Client
 * 
 * @returns
 */
function createClient() {
    var date = getDateValue();
    var crew = getCrewValue();
    var updates = {};

    firstName = document.querySelector('#firstName').textContent;
    lastName = document.querySelector('#lastName').textContent;

    if (!isValid(firstName, lastName, address, city, state, zip, date, crew)) {
        return Promise.resolve('error');
    }
    // Get a key for a new client.

    var newJobKey = database.ref().child('job').push().key;

    var clientData = {
        firstName: firstName.trim(),
        lastName: lastName,
        address: address,
        city: city,
        state: state,
        zip: zip,
        township: getTownshipValue()
    };

    Object.keys(clientData).forEach(function (key) {
        if (clientData[key] === '' || clientData[key] === false) {
            delete clientData[key];
        }
    });

    return checkIfClientExist(firstName, lastName).then(function (client) {
        var newClientKey = void 0;

        if (client) {
            newClientKey = client.key;
        } else {
            newClientKey = database.ref().child('clients').push().key;
        }

        return getJobCount().then(function (count) {

            var jobData = {
                assigned: crew,
                client: newClientKey,
                date: date,
                internal: false,
                inspection: false,
                status: 0,
                order: count + 1
            };

            updates['/clients/' + newClientKey] = clientData;
            updates['/jobs/' + newJobKey] = jobData;

            updateJobCount(count + 1);

            return database.ref().update(updates);
        });
    });
}

/**
 * Get Township Value
 * 
 * @returns {string}
 */
function getTownshipValue() {
    return $('.ui.dropdown.townships').dropdown('get value');
}

/**
 * Get Date Value
 * 
 * @returns {number}
 */
function getDateValue() {
    return selectedDates;
}

/**
 * Get Crew Value
 * 
 * @returns {string}
 */
function getCrewValue() {
    return $('.ui.dropdown.crews').dropdown('get value');
}

/**
 * Load Townships into select drop down
 * 
 */
function loadTownships() {
    firebase.database().ref('/townships/').once('value').then(function (snapshot) {
        var townships = snapshot.val();
        var dropdown = document.querySelector('.ui.search.dropdown.townships');
        var docfrag = document.createDocumentFragment();
        var townshipsList = townships[Object.keys(townships)[0]];

        townshipsList.forEach(function (township) {
            var option = document.createElement('option');
            option.innerText = township.trim();
            option.setAttribute('value', township.trim());
            docfrag.appendChild(option);
        });
        dropdown.appendChild(docfrag);
        $('.ui.dropdown.townships').dropdown();
    });
}

function getJobCount() {
    return firebase.database().ref('/jobCount/').once('value').then(function (snapshot) {
        return snapshot.val().value;
    });
}

function updateJobCount(count) {
    var update = {};
    update['/jobCount/value'] = count;
    database.ref().update(update);
}

function checkIfClientExist(_firstName, _lastName) {

    return firebase.database().ref('/clients/').once('value').then(function (snapshot) {
        var clients = snapshot.val();
        var keys = clients ? Object.keys(clients) : [];

        var clientsList = keys.reduce(function (acc, key) {
            var _clients$key = clients[key],
                firstName = _clients$key.firstName,
                lastName = _clients$key.lastName;

            acc.push({ firstName: firstName, lastName: lastName, key: key });
            return acc;
        }, []);

        return clientsList.find(function (c) {
            var firstName = c.firstName,
                lastName = c.lastName;

            return firstName === _firstName && _lastName === lastName;
        });
    });
}

function generateCrewDropDown() {
    firebase.database().ref('/crews/').once('value').then(function (snapshot) {
        var crews = snapshot.val();
        var dropdown = document.querySelector('.ui.search.dropdown.crews');
        var docfrag = document.createDocumentFragment();
        var keys = Object.keys(crews);

        var crewsList = keys.reduce(function (acc, key) {
            acc.push(crews[key].name);
            return acc;
        }, []);

        crewsList.forEach(function (crew, index) {
            var option = document.createElement('option');
            option.innerText = crew;
            option.setAttribute('value', keys[index]);
            docfrag.appendChild(option);
        });

        dropdown.appendChild(docfrag);

        $('.ui.dropdown.crews').dropdown();
    });
}

/**
 * Login into Firebase
 * 
 * @param {any} email 
 * @param {any} password 
 */
function login(email, password) {
    firebase.auth().signInWithEmailAndPassword(email, password).catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode === 'auth/wrong-password') {
            alert('Wrong password.');
        } else {
            alert(errorMessage);
        }
        console.log(error);
    });
}

/**
 * Displays network activity loader
 * 
 */
function loader() {
    var loader = document.querySelector('.ui.inverted.dimmer');
    loader.classList.toggle('active');
}

/**
 * Checks each argument to see if it is valid
 * 
 * @returns {any[]}
 */
function isValid() {
    var isValidList = Array.from(arguments).filter(function (arg) {
        var isValid = void 0;
        if (Array.isArray(arg)) {
            isValid = arg.length > 0;
        } else {
            isValid = arg !== '' || arg !== undefined;
        }
        return !isValid;
    });
    return isValidList.length === 0;
}

/**
 * Reset the display
 * 
 */
function resetDisplay() {
    window.close();
    fullName = undefined, address = undefined, city = undefined, state = undefined, zip = undefined, selectedDates = undefined, jobOrderValue = undefined, conditionA = undefined, conditionB = undefined, conditionC = undefined;

    createJobElement.setAttribute('style', 'display:none');
    requestClientDataElement.setAttribute('style', 'display:block');
    cardContent.setAttribute('style', 'display:none');
}