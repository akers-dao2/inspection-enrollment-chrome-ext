/* eslint-disable */
'use strict';

function extractName() {
  var clientName = document.querySelectorAll('#PageContent_lblClientName');
  var clientPageHeaderEl = document.querySelectorAll('.client-page-header :nth-child(1) div.col-10');
  clientName = Array.from(clientName);
  clientPageHeaderEl = Array.from(clientPageHeaderEl);
  var clientInfo = clientName.concat(clientPageHeaderEl);
  return clientInfo.map(function (client) {
    return client.textContent.trim();
  });
}

chrome.extension.onRequest.addListener(function (request, sender, sendResponse) {
  sendResponse(extractName());
});